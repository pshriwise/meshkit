Version 1.3  (SIGMA v1.1)
=========================

Fixes:
------

  1) Fixes and new enhancements to PostBL, using new mesh quality support from MOAB.
  2) Name all Neumann sets created by CoreGen (RGG).
  3) Fixes to building make_watertight on OSX.
  4) Fixed several warnings.
  5) Fixes to shared builds and make distcheck.
  6) Bug fixes for rectangular assembly arrangement in CoreGen.

Enhancements:
-------------

  1) Improvements to configure script for Mesquite, TetGen etc.
  2) New RGG example with shifting material and Neumann sets for creating new assemblies.

Version 1.2  (SIGMA v1.0)
=========================
See details on compatibility with dependent libraries in NEWS section of http://sigma.mcs.anl.gov

A. BUG FIXES
------------
  1) Various RGG (AssyGen/CoreGen) bugfixes and enhancements: common keywords inputs, superblocks,
     automated multiple file creation, automatic mesh size specification, scheme spefication for hole surfaces etc.
  2) Fixes to OneToOneSweep: new test case added
  3) Fixes to PostBL along with addition of methods to create mixed element meshes.
  4) Fixes to quadmesher and removed dependency on BOOST.

B. NEW FEATURES
---------------
  1) A new version of make_watertight for sealing faceted CAD Geometry, see page: http://sigma.mcs.anl.gov/?p=901
     Note: Use with MOAB 4.7.0 or higher.
     A program called check_watertight for evaluating model integrity for use with ray-firing has been included as well.
  2) RGG AssyGen feature to "CreateFiles" and "CreateMatFiles". CoreGen feature "same_as" to shift material/boundaries while loading the same mesh file.
  3) Load file with geometry group sets (see: test/core/test_load_grps.cpp)
  4) Routines with 21 options for quad mesh cleanup have been added (test/algs/test_quadmeshcleanup)
  5) RGG GUI has been developed by Kitware and is available for download from: http://cmb.kitware.com/CMB/resources/software.html
      CMB website: http://cmb.kitware.com/CMB/project/cmbnuclear.html

C. API CHANGES
--------------
  1) Fixes to get_entities_by_dimension and get_entities_by_handle.
     These functions will now return ModelEnts of dimension 4 if requested.
     If the dimension passed in is -1, the function will return all ModelEnts in the MK instance.
  2) Correction to the ModelEnt constructor for iGeom EntSets.

D. FUTURE WORK
--------------
  1) B-Rep(OCC)-based mesh generation, Solid Geometry Mesher-based on CGM, this will add the capability of
     meshing and manipulating solid geometry models based on OpenCascade and CUBIT.
  2) Open source tri-mesher
  3) Integration of verdict-mesh quality evalulation library
  4) Interval assignment integration with existing algorithms
  5) New parallel schemes and optimization of RGG tools: AssyGen and CoreGen.
  6) Parallel mesh generation

Version 1.1
=====================================
This version has been tested on Ubuntu 10.04, 12.04 and Mac OS X 10.6.8 to 10.9.
Some dependencies may have restrictive license.

The following version of dependencies are found to be compatible with MeshKit v 1.1
MPICH2 1.4.1p1, 1.5, open-mpi 1.6.4
MOAB Git SHAR1: bd52ba12517416f4b6d2162696a41583b73d52ed
CGM  https://git.mcs.anl.gov/repos/ITAPS/cgm/tags/13.1.1
LASSO https://git.mcs.anl.gov/repos/ITAPS/Lasso/trunk Rev: 6103

TRIANGLE: Triangle provides a 2D triangle mesh generation algorithm.

CAMAL/CUBIT 5.1.0: Sandia National Lab’s proprietary CAMAL library provides access to tetrahedral, tri-advance and paving algorithms.

NetGen 4.9.13: An open source automatic tetrahedral mesh generation library.

Mesquite: Open source mesh optimization package MESQUITE

IPOPT: 3.10.0 (Coin), MA27 solver http://www.hsl.rl.ac.uk/download/coinhsl-archive/2011.10.03/
Ipopt an optimization library used by interval assignment algorithm developed in MeshKit.
